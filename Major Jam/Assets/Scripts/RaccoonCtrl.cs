﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaccoonCtrl : InteractableCtrl {

    [SerializeField] private Animator animator;
    [SerializeField] private AnimationCtrl animationCtrl;

    [SerializeField] private AudioClip hissSFX;
    [SerializeField] private AudioClip bushesSFX;

    IEnumerator sfxRoutine = null;

    private bool started = false;

    void Start() {
        ConnectListeners();
        started = true;
    }

    protected override void OnEnable() {
        if (started) {
            ConnectListeners();
        }
        base.OnEnable();
    }

    void ConnectListeners() {
        animationCtrl.OnAnimationEvent.AddListener(OnAnimationEvent);
        animationCtrl.OnSFX.AddListener(OnSFXEvent);
    }

    public void RunAway() {
        animator.Play("Bushes Exit");
    }

    void OnAnimationEvent(string eventName) {
        if (eventName == "Destroy") {
            if (sfxRoutine != null) {
                StopCoroutine(sfxRoutine);
            }
            Destroy(this.gameObject);
        }
    }

    void OnSFXEvent(string sfxName) {
        if (sfxName == "Hiss") {
            sfxCtrl.PlayOneShot(hissSFX);
            float originalVolume = sfxCtrl.volume;
            sfxRoutine = Utils.Delay(0.5f, delegate {
                sfxCtrl.volume -= 0.1f;
                sfxCtrl.PlayOneShot(hissSFX);
                sfxRoutine = Utils.Delay(0.5f, delegate {
                    sfxCtrl.volume = originalVolume;
                });
                StartCoroutine(sfxRoutine);
            });
            StartCoroutine(sfxRoutine);
        } else if (sfxName == "Bushes") {
            sfxRoutine = PlayBushesSFX();
            StartCoroutine(sfxRoutine);
        }
    }

    IEnumerator PlayBushesSFX() {
        int count = 0;
        float originalVolume = sfxCtrl.volume;
        while (count < 4) {
            sfxCtrl.volume -= 0.05f;
            sfxCtrl.PlayOneShot(bushesSFX);
            count += 1;
            yield return new WaitForSeconds(0.2f);
        }

        sfxCtrl.volume = originalVolume;
    }

}
