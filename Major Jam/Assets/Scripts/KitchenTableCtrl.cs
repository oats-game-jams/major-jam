﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenTableCtrl : InteractableCtrl {

    [SerializeField] private Transform mail;

    public override void UpdateState(GameState gameState) {
        mail.gameObject.SetActive(gameState.putMailOnTable);
    }

    public override DialogData[] Inspect(GameState gameState) {
        List<DialogData> dataList = new List<DialogData>();
        DialogData data;

        if (gameState.putMailOnTable) {
            data = new DialogData(DialogData.DialogType.Dog, "I should check the rest of the house to see what else I can do.");
        } else if (gameState.pickedUpMail) {
            data = new DialogData(DialogData.DialogType.Dog, "I can HOP UP and put the mail on the table.");
        } else if (gameState.droppedMail) {
            data = new DialogData(DialogData.DialogType.Dog, "My best friend reads the paper here...I can check the mail I knocked out of the mailbox.");
        } else {
            data = new DialogData(DialogData.DialogType.Dog, "My best friend reads the paper here...I should check the mailbox.");
        }

        dataList.Add(data);

        return dataList.ToArray();
    }

    public void PlaceMail() {
        mail.gameObject.SetActive(true);
    }

}
