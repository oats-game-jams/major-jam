﻿using UnityEngine;
using UnityEngine.Events;

public class BarkTriggerCtrl : MonoBehaviour {

    public class BarkTriggerEvent : UnityEvent<BarkTriggerCtrl> {}

    public enum IDs { FrontDoor, Raccoon, Attic }

    [SerializeField] private IDs id;
    public IDs ID { get { return id; }}

    public void TriggerEvent() {
        Destroy(this.gameObject);
    }

}
