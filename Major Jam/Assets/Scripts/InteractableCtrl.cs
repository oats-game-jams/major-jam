﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum InteractionAction { UnlockBark, UnlockLick, DropMail, TakeMail }

public class InteractableCtrl : MonoBehaviour {

    public class InteractionActionEvent : UnityEvent<InteractionAction> {}
    public class InteractableEvent : UnityEvent<InteractableCtrl> {}

    [SerializeField] protected string interactPrompt;
    public string InteractPrompt { get { return interactPrompt; }}

    [SerializeField] private List<DialogData> inspectDialogData = new List<DialogData>();
    public DialogData[] InspectDialogData { get { return inspectDialogData.ToArray(); }}

    [SerializeField] private List<DialogData> barkDialogData = new List<DialogData>();
    public DialogData[] BarkDialogData { get { return barkDialogData.ToArray(); }}

    [SerializeField] private List<DialogData> hopUpDialogData = new List<DialogData>();
    public DialogData[] HopUpDialogData { get { return hopUpDialogData.ToArray(); }}

    [SerializeField] private List<DialogData> crawlUnderDialogData = new List<DialogData>();
    public DialogData[] CrawlUnderDialogData { get { return crawlUnderDialogData.ToArray(); }}

    [SerializeField] private List<DialogData> lickDialogData = new List<DialogData>();
    public DialogData[] LickDialogData { get { return lickDialogData.ToArray(); }}

    [SerializeField] protected new Collider2D collider;

    [SerializeField] protected AudioSource sfxCtrl;
    [SerializeField] protected AudioClip sfx;

    public InteractableEvent OnTouch;
    public InteractableEvent OnTouchEnd;
    public InteractionActionEvent OnInteractAction;

    void Awake() {
        OnTouch = new InteractableEvent();
        OnTouchEnd = new InteractableEvent();
        OnInteractAction = new InteractionActionEvent();
    }

    protected virtual void OnEnable() {
        collider.enabled = true;
    }

    void OnDisable() {
        collider.enabled = false;

        OnTouch.RemoveAllListeners();
        OnTouchEnd.RemoveAllListeners();
        OnInteractAction.RemoveAllListeners();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            OnTouch.Invoke(this);
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Player") {
            OnTouchEnd.Invoke(this);
        }
    }

    public void PlaySFX() {
        if (sfxCtrl != null && sfx != null) {
            sfxCtrl.PlayOneShot(sfx);
        }
    }

    public virtual void UpdateState(GameState gameState) {}

    public virtual DialogData[] Inspect(GameState gameState) {
        return this.InspectDialogData;
    }

    public DialogData[] BarkAt(GameState gameState) {
        return this.BarkDialogData;
    }

    public DialogData[] HopUpOn(GameState gameState) {
        return this.HopUpDialogData;
    }

    public DialogData[] CrawlUnder(GameState gameState) {
        return this.CrawlUnderDialogData;
    }

    public DialogData[] Lick(GameState gameState) {
        return this.LickDialogData;
    }

}

[Serializable]
public class DialogData {

    public enum DialogType { Dog, Ghost }

    [SerializeField] private DialogType type = DialogType.Dog;
    public DialogType Type { get { return type; }}

    [SerializeField] private string text;
    public string Text { get { return text; }}

    public DialogData (DialogType type, string text) {
        this.type = type;
        this.text = text;
    }

}
