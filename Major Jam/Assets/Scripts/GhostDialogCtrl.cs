﻿using System.Collections;
using TMPro;
using UnityEngine;

public class GhostDialogCtrl : MonoBehaviour {

    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private TextMeshProUGUI textUI;
    [SerializeField] private TextMeshProUGUI textUIBG;

    public bool IsVisible { get { return canvasGroup.alpha > 0; }}

    private bool fadeInProgress = false;
    public bool FadeInProgress { get { return fadeInProgress; }}

    IEnumerator fadeRoutine = null;

    void OnDisable() {
        if (fadeRoutine != null) {
            StopCoroutine(fadeRoutine);

            canvasGroup.alpha = 0;
        }
    }

    // Update is called once per frame
    public void Show(string newText = "", bool instant = false) {
        if (canvasGroup.alpha == 1) return;

        fadeInProgress = true;

        if (fadeRoutine != null) {
            StopCoroutine(fadeRoutine);
        }

        if (newText != "") {
            textUI.text = newText;
            textUIBG.text = newText;
        }

        fadeRoutine = Utils.FadeToAlpha(canvasGroup, 1, instant ? 0f : 0.5f, delegate {
            fadeInProgress = false;
        });
        StartCoroutine(fadeRoutine);
    }

    public void Hide(bool instant = false) {
        if (canvasGroup.alpha == 0) return;

        fadeInProgress = true;

        if (fadeRoutine != null) {
            StopCoroutine(fadeRoutine);
        }

        fadeRoutine = Utils.FadeToAlpha(canvasGroup, 0, instant ? 0f : 0.5f, delegate {
            fadeInProgress = false;
        });
        StartCoroutine(fadeRoutine);
    }

}
