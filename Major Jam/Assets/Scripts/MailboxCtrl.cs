﻿using System.Collections.Generic;
using UnityEngine;

public class MailboxCtrl : InteractableCtrl {

    [SerializeField] private Animator animator;
    [SerializeField] private AnimationCtrl animationCtrl;

    [SerializeField] private Transform mail;

    [SerializeField] private string beforeMail;
    [SerializeField] private string duringMail;
    [SerializeField] private string afterMail;

    public override DialogData[] Inspect(GameState gameState) {
        List<DialogData> dataList = new List<DialogData>();
        DialogData data;

        if (gameState.pickedUpMail) {
            data = new DialogData(DialogData.DialogType.Dog, afterMail);
        } else if (gameState.droppedMail) {
            data = new DialogData(DialogData.DialogType.Dog, duringMail);
            mail.gameObject.SetActive(false);
            OnInteractAction.Invoke(InteractionAction.TakeMail);
        } else {
            data = new DialogData(DialogData.DialogType.Dog, beforeMail);
        }

        dataList.Add(data);

        return dataList.ToArray();
    }

    void Start() {
        animationCtrl.OnAnimationEvent.AddListener(OnAnimationEvent);
    }

    public override void UpdateState(GameState gameState) {
        if (gameState.droppedMail && !gameState.pickedUpMail) {
            animator.Play("Empty");
            mail.gameObject.SetActive(true);
        } else if (gameState.pickedUpMail) {
            animator.Play("Empty");
            mail.gameObject.SetActive(false);
        } else {
            animator.Play("Idle");
            mail.gameObject.SetActive(false);
        }
    }

    public void DropMail() {
        animator.Play("Drop Mail");
    }

    void OnAnimationEvent(string eventName) {
        if (eventName == "Animation End") {
            OnInteractAction.Invoke(InteractionAction.DropMail);
            mail.gameObject.SetActive(true);
            animator.Play("Empty");
        }
    }

}
