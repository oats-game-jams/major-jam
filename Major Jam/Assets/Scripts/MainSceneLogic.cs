﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class MainSceneLogic : MonoBehaviour {

    [SerializeField] private PCCtrl pc;

    [SerializeField] private DialogBoxCtrl dialogBox;

    // camera for following PC
    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    // bounds for within which camera movement is limited
    [SerializeField] private CinemachineConfiner cameraBoundsConfiner;
    [SerializeField] private CanvasGroup cameraFader;

    [SerializeField] private CanvasGroup endCanvasGroup;

    [SerializeField] private CanvasGroup actionPrompts;

    [SerializeField] private ActionPromptCtrl interactPrompt;
    [SerializeField] private ActionPromptCtrl barkPrompt;
    [SerializeField] private ActionPromptCtrl lickPrompt;

    [SerializeField] private GhostDialogCtrl ghostDialogBox;

    [SerializeField] private RaccoonCtrl raccoon;
    [SerializeField] private Transform backDoggyDoor;

    [SerializeField] private MailboxCtrl mailbox;
    [SerializeField] private KitchenTableCtrl kitchenTableCtrl;

    [SerializeField] private List<MapCtrl> maps;
    private MapCtrl currentMap;

    [SerializeField] private AudioSource musicCtrl;
    [SerializeField] private AudioSource sfxCtrl;
    private IEnumerator musicFadeRoutine;

    [SerializeField] private AudioClip mailboxSFX;
    [SerializeField] private AudioClip newspaperSFX;

    private GameState gameState = new GameState();

    private List<InteractableCtrl> interactables = new List<InteractableCtrl>();

    private bool dialogInProgress = false;
    private DialogData[] activeDialog;
    private int dialogIdx = 0;
    private bool transitioningMap = false;

    private bool dialogControlsOnly = false;

    private bool queueAttic = false;

    private bool ending = false;
    private bool queueEnding = false;

    private InteractableCtrl currentInteractable = null;

    void Start() {
        cameraFader.alpha = 1;

        dialogBox.Close();

        StartGame();
    }

    void StartGame() {
        dialogControlsOnly = true;

        ghostDialogBox.Hide(true);
        interactPrompt.Hide(true);
        barkPrompt.Hide(true);
        lickPrompt.Hide(true);

        // activate first map
        MapCtrl.Keys firstMapKey = MapCtrl.Keys.FrontYard;
        ActivateMap(GetMapByKey(firstMapKey));

        pc.transform.position = currentMap.GetSpawnPoint(0);

        FadeIn(delegate {
            if (!musicCtrl.isPlaying) {
                musicCtrl.volume = 0f;
                musicCtrl.Play();
            }

            musicFadeRoutine = Utils.FadeIn(musicCtrl, 0.5f, 1);
            StartCoroutine(musicFadeRoutine);

            StartCoroutine(Utils.Delay(1.0f, delegate {
                dialogInProgress = true;
                DialogData introDialog1 = new DialogData(DialogData.DialogType.Dog, "All the cities are quiet now... I'm not sure what happened.");
                DialogData introDialog2 = new DialogData(DialogData.DialogType.Dog, "I was waiting for my best friend to come get me, but I guess I need to go get him...");
                activeDialog = new DialogData[2];
                activeDialog[0] = introDialog1;
                activeDialog[1] = introDialog2;
                dialogBox.Open();
                dialogBox.UpdateText(activeDialog[0].Text);
            }));
        });
    }

    void TransitionToMap(MapCtrl map, int spawnPointIdx = 0) {
        transitioningMap = true;

        if (pc != null) {
            pc.DisableMovement();
        }

        interactables.Clear();

        FadeOut(0.25f, delegate {
            ActivateMap(map);

            pc.transform.position = currentMap.GetSpawnPoint(spawnPointIdx);

            StartCoroutine(Utils.Delay(1.0f, delegate {
                FadeIn(delegate {
                    transitioningMap = false;

                    if (map.MapID == MapCtrl.Keys.Attic) {
                        // we at the enddddd boyyyyy
                        StartCoroutine(Utils.Delay(0.25f, delegate {
                            dialogInProgress = true;
                            DialogData doorDialog = new DialogData(DialogData.DialogType.Dog, "I should call for my best friend!");
                            activeDialog = new DialogData[1];
                            activeDialog[0] = doorDialog;
                            dialogBox.Open();
                            dialogBox.UpdateText(activeDialog[0].Text);
                        }));
                    } else {
                        pc.EnableMovement();
                    }

                });
            }));

        });
    }

    void ActivateMap(MapCtrl map) {
        currentMap = map;

        for (int i = 0; i < maps.Count; i++) {
            maps[i].gameObject.SetActive(maps[i] == currentMap);
        }

        currentMap.RestoreState(gameState);

        currentMap.OnTouchInteractable.AddListener(TouchInteractable);
        currentMap.OnTouchEndInteractable.AddListener(TouchEndInteractable);
        currentMap.OnInteractionAction.AddListener(HandleInteractionAction);

        cameraBoundsConfiner.m_BoundingShape2D = currentMap.CameraBounds;
        // lets camera use new bounds (one of those important, undocumented things)
        cameraBoundsConfiner.InvalidatePathCache();
    }

    void TouchInteractable(InteractableCtrl interactable) {
        string interactPromptText = "Inspect";
        if (interactable.InteractPrompt != "") {
            interactPromptText = interactable.InteractPrompt;
        }
        interactPrompt.Show(interactPromptText);

        interactables.Add(interactable);

        // show crawl dialog
        if (pc.IsCrawling && interactable.CrawlUnderDialogData.Length > 0 && !dialogInProgress) {
            pc.DisableMovement();
            dialogInProgress = true;
            interactable.PlaySFX();
            ghostDialogBox.Show(interactable.CrawlUnderDialogData[0].Text);
        }
    }

    void TouchEndInteractable(InteractableCtrl interactable) {
        interactables.Remove(interactable);

        if (interactables.Count <= 0) {
            interactPrompt.Hide();
        }
    }

    void HandleInteractionAction(InteractionAction action) {
        if (action == InteractionAction.UnlockBark) {
            UnlockBark();
        } else if (action == InteractionAction.UnlockLick) {
            UnlockLick();
        } else if (action == InteractionAction.DropMail) {
            gameState.droppedMail = true;
        } else if (action == InteractionAction.TakeMail) {
            gameState.pickedUpMail = true;
        }
    }

    void Update() {
        if (ending) return;

        if (!transitioningMap && !ghostDialogBox.FadeInProgress) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                if (dialogBox.IsOpen || ghostDialogBox.IsVisible) {
                    AdvanceDialog();
                } else {
                    if (!dialogControlsOnly) {
                        Inspect();
                    }
                }
            } else if (!dialogControlsOnly && gameState.barkUnlocked && Input.GetKeyDown(KeyCode.F)) {
                pc.Bark();

                StartCoroutine(Utils.Delay(0.5f, delegate {
                    BarkTriggerCtrl barkTrigger = pc.GetNearestBarkTrigger();
                    if (barkTrigger != null) {
                        HandleBarkTrigger(barkTrigger);
                    } else {
                        InteractableCtrl interactable = pc.GetBarkTarget();
                        if (interactable != null) {
                            pc.DisableMovement();
                            activeDialog = interactable.BarkAt(gameState);
                            if (activeDialog.Length > 0) {
                                currentInteractable = interactable;
                                AdvanceDialog();
                            } else {
                                if (!dialogInProgress) {
                                    pc.EnableMovement();
                                }
                            }
                        }
                    }
                }));
            } else if (!dialogControlsOnly && (gameState.lickUnlocked && Input.GetKeyDown(KeyCode.X))) {
                pc.Lick();

                InteractableCtrl interactable = pc.GetLickTarget();
                if (interactable != null) {
                    pc.DisableMovement();
                    activeDialog = interactable.Lick(gameState);
                    if (activeDialog.Length > 0) {
                        currentInteractable = interactable;
                        AdvanceDialog();
                    } else {
                        if (!dialogInProgress) {
                            pc.EnableMovement();
                        }
                    }
                }
            } else if (!dialogControlsOnly && (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))) {
                pc.HopUp();

                StartCoroutine(Utils.Delay(0.4f, delegate {

                    HopUpTriggerCtrl hopUpTrigger = pc.GetNearestHopUpTrigger();
                    if (hopUpTrigger != null) {
                        HandleHopUpTrigger(hopUpTrigger);
                    } else {
                        InteractableCtrl interactable = pc.GetBarkTarget();
                        if (interactable != null) {
                            pc.DisableMovement();
                            activeDialog = interactable.HopUpOn(gameState);
                            if (activeDialog.Length > 0) {
                                currentInteractable = interactable;
                                AdvanceDialog();
                            } else {
                                if (!dialogInProgress) {
                                    pc.EnableMovement();
                                }
                            }
                        }
                    }
                }));
            }
        }
    }

    void HandleBarkTrigger(BarkTriggerCtrl barkTrigger) {
        if (barkTrigger == null) return;

        if (barkTrigger.ID == BarkTriggerCtrl.IDs.FrontDoor) {
            dialogControlsOnly = true;
            // front door cutscene
            pc.DisableMovement();

            StartCoroutine(Utils.Delay(2.0f, delegate {
                dialogInProgress = true;
                DialogData doorDialog = new DialogData(DialogData.DialogType.Dog, "Maybe he didn’t hear me... There should be a doggy door in the backyard I can use instead.");
                activeDialog = new DialogData[1];
                activeDialog[0] = doorDialog;
                dialogBox.Open();
                dialogBox.UpdateText(activeDialog[0].Text);
                gameState.barkedAtFrontDoor = true;
            }));
        } else if (barkTrigger.ID == BarkTriggerCtrl.IDs.Raccoon) {
            dialogControlsOnly = true;
            pc.DisableMovement();

            raccoon.RunAway();

            StartCoroutine(Utils.Delay(2f, delegate {
                backDoggyDoor.gameObject.SetActive(true);
                currentMap.AddInteractableEventToInteractable(backDoggyDoor);
                dialogControlsOnly = false;
                pc.EnableMovement();
            }));
        } else if (barkTrigger.ID == BarkTriggerCtrl.IDs.Attic) {
            dialogControlsOnly = true;
            pc.DisableMovement();

            queueEnding = true;

            // otherwise, just show description text
            dialogInProgress = true;
            DialogData doorDialog = new DialogData(DialogData.DialogType.Dog, "Maybe if I try again?");
            activeDialog = new DialogData[1];
            activeDialog[0] = doorDialog;
            dialogBox.Open();
            dialogBox.UpdateText(activeDialog[0].Text);
        }
        barkTrigger.TriggerEvent();
    }

    void HandleHopUpTrigger(HopUpTriggerCtrl hopUpTriggerCtrl) {
        if (hopUpTriggerCtrl == null) return;

        if (hopUpTriggerCtrl.ID == HopUpTriggerCtrl.IDs.Mailbox) {
            pc.DisableMovement();

            mailbox.DropMail();

            StartCoroutine(Utils.Delay(0.1f, delegate {
                sfxCtrl.PlayOneShot(mailboxSFX);
            }));

            StartCoroutine(Utils.Delay(0.3f, delegate {
                sfxCtrl.PlayOneShot(newspaperSFX);
            }));

            StartCoroutine(Utils.Delay(1.0f, delegate {
                pc.EnableMovement();
            }));
        } else if (hopUpTriggerCtrl.ID == HopUpTriggerCtrl.IDs.KitchenTable) {
            if (gameState.pickedUpMail) {
                pc.DisableMovement();
                gameState.putMailOnTable = true;
                kitchenTableCtrl.PlaceMail();

                StartCoroutine(Utils.Delay(0.25f, delegate {
                    dialogInProgress = true;
                    DialogData tableDialog = new DialogData(DialogData.DialogType.Dog, "I should check the rest of the house to see what else I can do.");
                    activeDialog = new DialogData[1];
                    activeDialog[0] = tableDialog;
                    dialogBox.Open();
                    dialogBox.UpdateText(activeDialog[0].Text);
                }));
            }
        } else if (hopUpTriggerCtrl.ID == HopUpTriggerCtrl.IDs.Bookcase) {
            queueAttic = true;

            dialogInProgress = true;
            DialogData tableDialog = new DialogData(DialogData.DialogType.Dog, "I think I can reach the attic by hopping up here.");
            activeDialog = new DialogData[1];
            activeDialog[0] = tableDialog;
            dialogBox.Open();
            dialogBox.UpdateText(activeDialog[0].Text);
        }

        hopUpTriggerCtrl.TriggerEvent();
    }

    void FixedUpdate() {
        if (ending) return;

        if (pc != null && !transitioningMap) {
            pc.Move(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
    }

    void Inspect() {
        if (pc != null) {
            InteractableCtrl interactable = pc.Inspect();
            if (interactable != null) {

                pc.DisableMovement();

                // is it an exit?
                ExitCtrl exit = interactable.GetComponent<ExitCtrl>();
                if (exit != null) {
                    MapCtrl map = GetMapByKey(exit.ToMap);
                    if (map == null) {
                        // may as well let the PC move
                        pc.EnableMovement();
                        Debug.LogError("Map not found!");
                    }
                    TransitionToMap(map, exit.ToSpawnPoint);
                } else {
                    // otherwise, just show description text
                    activeDialog = interactable.Inspect(gameState);
                    currentInteractable = interactable;
                    AdvanceDialog();
                }

            }
        }
    }

    void PlayEnding() {
        ending = true;

        pc.Bark(true);

        StartCoroutine(Utils.Delay(2.0f, delegate {
            FadeOut(3.0f, delegate {
                StartCoroutine(Utils.FadeOut(musicCtrl, 3.0f));
                StartCoroutine(Utils.FadeToAlpha(endCanvasGroup, 1, 1.0f));
            });
        }));
    }

    void AdvanceDialog() {
        pc.DisableMovement();

        if (!dialogInProgress) {
            dialogInProgress = true;

            if (activeDialog[dialogIdx].Type == DialogData.DialogType.Ghost) {
                if (currentInteractable != null) {
                    currentInteractable.PlaySFX();
                }
                ghostDialogBox.Show(activeDialog[dialogIdx].Text);
            } else {
                dialogBox.Open();
                dialogBox.UpdateText(activeDialog[dialogIdx].Text);
            }
        } else {
            if (dialogBox.IsOpen) {
                if (dialogBox.IsTyping) {
                    dialogBox.FinishTyping();
                } else {
                    dialogIdx += 1;

                    if (dialogIdx < activeDialog.Length) {
                        if (activeDialog[dialogIdx].Type == DialogData.DialogType.Ghost) {
                            dialogBox.Close();
                            if (currentInteractable != null) {
                                currentInteractable.PlaySFX();
                            }
                            ghostDialogBox.Show(activeDialog[dialogIdx].Text);
                        } else {
                            dialogBox.UpdateText(activeDialog[dialogIdx].Text);
                        }
                    } else {
                        dialogInProgress = false;
                        dialogIdx = 0;
                        dialogBox.Close();
                        activeDialog = new DialogData[0];
                        if (queueEnding) {
                            PlayEnding();
                        } else if (queueAttic) {
                            queueAttic = false;
                            TransitionToMap(GetMapByKey(MapCtrl.Keys.Attic));
                        } else {
                            currentInteractable = null;
                            dialogControlsOnly = false;
                            pc.EnableMovement();
                        }
                    }
                }
            } else if (ghostDialogBox.IsVisible) {
                dialogIdx += 1;

                if (dialogIdx < activeDialog.Length) {
                    if (activeDialog[dialogIdx].Type == DialogData.DialogType.Ghost) {
                        if (currentInteractable != null) {
                            currentInteractable.PlaySFX();
                        }
                        ghostDialogBox.Show(activeDialog[dialogIdx].Text);
                    } else {
                        ghostDialogBox.Hide();
                        dialogBox.Open();
                        dialogBox.UpdateText(activeDialog[dialogIdx].Text);
                    }
                } else {
                    dialogInProgress = false;
                    dialogIdx = 0;
                    currentInteractable = null;
                    ghostDialogBox.Hide();
                    activeDialog = new DialogData[0];
                    dialogControlsOnly = false;
                    pc.EnableMovement();
                }
            }
        }
    }

    MapCtrl GetMapByKey(MapCtrl.Keys mapKey) {
        MapCtrl map = null;

        for (int i = 0; i < maps.Count; i++) {
            if (maps[i].MapID == mapKey) {
                map = maps[i];
                break;
            }
        }

        return map;
    }

    void FadeIn(UnityAction OnComplete = null) {
        StartCoroutine(Utils.FadeToAlpha(cameraFader, 0, 0.25f, OnComplete));
    }

    void FadeOut(float duration = 0.25f, UnityAction OnComplete = null) {
        StartCoroutine(Utils.FadeToAlpha(cameraFader, 1, duration, OnComplete));
    }

    void UnlockBark() {
        gameState.barkUnlocked = true;
        barkPrompt.Show();
    }

    void UnlockLick() {
        gameState.lickUnlocked = true;
        lickPrompt.Show();
    }

}
