﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCtrl : MonoBehaviour {

    public enum Keys { FrontYard, BackYard, LivingRoom, Kitchen, Bathroom, Bedroom, Attic }

    [SerializeField] private Keys mapID;
    public Keys MapID { get { return mapID; }}

    [SerializeField] private Collider2D cameraBounds;
    public Collider2D CameraBounds { get { return cameraBounds; }}

    [SerializeField] private Transform spawnPointContainer;
    [SerializeField] private Transform interactablesContainer;
    [SerializeField] private Transform barkTriggersContainer;

    public InteractableCtrl.InteractableEvent OnTouchInteractable;
    public InteractableCtrl.InteractableEvent OnTouchEndInteractable;
    public InteractableCtrl.InteractionActionEvent OnInteractionAction;

    private bool started = false;

    void Awake() {
        OnTouchInteractable = new InteractableCtrl.InteractableEvent();
        OnTouchEndInteractable = new InteractableCtrl.InteractableEvent();
        OnInteractionAction = new InteractableCtrl.InteractionActionEvent();
    }

    void Start() {
        AddInteractableEvents();
        started = true;
    }

    void OnEnable() {
        if (started) {
            AddInteractableEvents();
        }
    }

    public void RestoreState(GameState gameState) {
        if (interactablesContainer == null) return;

        for (int i = 0; i < interactablesContainer.childCount; i++) {
            InteractableCtrl interactable = interactablesContainer.GetChild(i).GetComponent<InteractableCtrl>();
            interactable.UpdateState(gameState);
        }
    }

    void AddInteractableEvents() {
        if (interactablesContainer == null) return;

        for (int i = 0; i < interactablesContainer.childCount; i++) {
            InteractableCtrl interactable = interactablesContainer.GetChild(i).GetComponent<InteractableCtrl>();
            AddInteractableEventToInteractable(interactable);
        }
    }

    public void AddInteractableEventToInteractable(InteractableCtrl interactable, bool toggleActive = false) {
        if (interactable == null || !interactable.gameObject.activeSelf) return;

        interactable.OnTouch.AddListener(OnTouchInteractable.Invoke);
        interactable.OnInteractAction.AddListener(OnInteractionAction.Invoke);
        interactable.OnTouchEnd.AddListener(OnTouchEndInteractable.Invoke);
    }

    public void AddInteractableEventToInteractable(Transform interactableTransform) {
        InteractableCtrl interactable = interactableTransform.GetComponent<InteractableCtrl>();
        if (interactable != null) {
            AddInteractableEventToInteractable(interactable, true);
        }
    }

    void OnDisable() {
        OnTouchInteractable.RemoveAllListeners();
        OnTouchEndInteractable.RemoveAllListeners();
        OnInteractionAction.RemoveAllListeners();
    }

    public Vector3 GetSpawnPoint(int idx) {
        if (idx < 0 || idx >= spawnPointContainer.childCount) {
            idx = 0;
        }

        Transform spawnPoint = spawnPointContainer.GetChild(idx);

        return spawnPoint.position;
    }

}
