﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogBoxCtrl : MonoBehaviour {

    [SerializeField] private CanvasGroup dialogBoxContainer;
    [SerializeField] private TextMeshProUGUI textUI;

    [SerializeField] private CanvasGroup textProgressIndicatorUI;
    [SerializeField] private Image textProgressIndicatorImage;
    [SerializeField] private Sprite textProgressSpriteMore;
    [SerializeField] private Sprite textProgressSpriteFinal;

    [SerializeField] private float typingSpeed = 0.02f;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip textSFX;

    public bool IsOpen { get { return dialogBoxContainer.alpha == 1; }}

    private bool typingInProgress;
    public bool IsTyping { get { return typingInProgress; }}

    private string textToType;
    private bool finalLine;

    // hacky way to "wrap" text is to have it all typed, then insert transparent text
    // for any text that isn't yet shown.
    private string hiddenTextTagStart = "<color=#00000000>";
    private string hiddenTextTagEnd = "</color>";

    private IEnumerator typingRoutine;
    private IEnumerator typingSFXRoutine;

    private UnityAction OnTypingComplete = null;

    void OnEnable() {
        typingInProgress = false;

        HideTextProgressUI();
    }

    void OnDisable() {
        StopTyping();

        typingInProgress = false;
    }

    public void Open() {
        // reset text before opening
        StopTyping();
        ClearText();

        HideTextProgressUI();

        dialogBoxContainer.alpha = 1;
    }

    public void Close() {
        StopTyping();

        HideTextProgressUI();

        dialogBoxContainer.alpha = 0;

        // reset text after closing
        ClearText();
    }

    // finalText: whether this is the final line of text to be displayed
    public void UpdateText(string newText, bool skipTyping = false, bool finalText = true, UnityAction OnTypingComplete = null) {
        StopTyping();

        HideTextProgressUI();

        this.OnTypingComplete = OnTypingComplete;

        if (textUI != null) {
            if (skipTyping) {
                SetText(newText);
                if (textProgressIndicatorUI != null) {
                    if (finalText) {
                        textProgressIndicatorImage.sprite = textProgressSpriteFinal;
                    } else {
                        textProgressIndicatorImage.sprite = textProgressSpriteMore;
                    }
                    ShowTextProgressUI();
                }
            } else {
                TypeText(newText, finalText);
            }
        }
    }

    public void TypeText(string newText, bool finalText = true) {
        StopTyping();

        typingInProgress = true;

        textToType = newText;
        finalLine = finalText;

        typingRoutine = StartTyping();
        StartCoroutine(typingRoutine);
    }

    public void FinishTyping() {
        if (!typingInProgress || typingRoutine == null) return;

        typingInProgress = false;

        StopCoroutine(typingRoutine);
        typingRoutine = null;

        // set text to full text
        SetText(textToType);

        if (textProgressIndicatorUI != null && textProgressIndicatorImage != null) {
            if (finalLine) {
                textProgressIndicatorImage.sprite = textProgressSpriteFinal;
            } else {
                textProgressIndicatorImage.sprite = textProgressSpriteMore;
            }
            ShowTextProgressUI();
        }

        if (OnTypingComplete != null) {
            OnTypingComplete();
        }
    }

    IEnumerator StartTyping() {
        if (textToType != null) {
            typingSFXRoutine = PlayTypeSFX();
            StartCoroutine(typingSFXRoutine);

            int textCursor = 0;

            while(textCursor < textToType.Length) {
                textCursor += 1;
                SetText(textToType.Substring(0, textCursor) + hiddenTextTagStart + textToType.Substring(textCursor) + hiddenTextTagEnd);
                yield return new WaitForSeconds(typingSpeed);
            }

            FinishTyping();
        } else {
            // TODO: better error?
            Debug.LogError("No text set for typing into dialog box.");
        }
    }

    void SetText(string newText) {
        textUI.text = newText;
    }

    void ClearText() {
        SetText("");
    }

    void StopTyping() {
        // stop typing routine
        if (typingRoutine != null) {
            StopCoroutine(typingRoutine);
        }
    }

    void ShowTextProgressUI() {
        if (textProgressIndicatorUI != null) {
            textProgressIndicatorUI.alpha = 1;
        }
    }

    void HideTextProgressUI() {
        if (textProgressIndicatorUI != null) {
            textProgressIndicatorUI.alpha = 0;
        }
    }

    IEnumerator PlayTypeSFX() {
        while (typingInProgress) {
            audioSource.volume = UnityEngine.Random.Range(0.2f, 0.35f);
            audioSource.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
            audioSource.PlayOneShot(textSFX);
            yield return new WaitForSeconds(0.08f);
        }
    }

}
