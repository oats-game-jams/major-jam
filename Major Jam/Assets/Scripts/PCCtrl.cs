﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PCCtrl : MonoBehaviour {

    [SerializeField] private Rigidbody2D body;
    [SerializeField] private new SpriteRenderer renderer;

    [SerializeField] private Animator animator;
    [SerializeField] private AnimationCtrl animationCtrl;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip barkSFX;

    // whether the player can move the PC via input
    private bool canMove = false;
    private bool crawling = false;
    public bool IsCrawling { get { return crawling; }}

    private float movementSpeed = 0.05f;
    private float walkSpeed = 0.05f;
    private float crawlSpeed = 0.03f;

    private int sign = 0;
    private int lastSign = 0;

    private bool animationLock = false;

    void Start() {
        animationCtrl.OnAnimationEvent.AddListener(OnAnimationEvent);
        animationCtrl.OnSFX.AddListener(OnSFXEvent);
    }

    /**
     * Moves the player sprite across the screen
     */
    public void Move(float horizontalInput, float verticalInput) {
        if (animationLock) return;

        if (canMove) {
            bool moving = horizontalInput != 0;
            crawling = verticalInput < 0;

            if (moving) {
                if (crawling) {
                    movementSpeed = crawlSpeed;
                } else {
                    movementSpeed = walkSpeed;
                }

                lastSign = sign;
                sign = Math.Sign(horizontalInput);
                this.transform.Translate(new Vector3(sign * movementSpeed, 0, 0));
                if (sign != lastSign) {
                    renderer.flipX = sign < 0;
                }
                if (crawling) {
                    movementSpeed = crawlSpeed;
                    animator.Play("Crawl");
                } else {
                    animator.Play("Walk");
                }
            } else {
                if (crawling) {
                    animator.Play("Crawl Idle");
                } else {
                    animator.Play("Idle");
                }
            }
        } else {
            if (crawling) {
                animator.Play("Crawl Idle");
            } else {
                animator.Play("Idle");
            }
        }
    }

    public void HopUp() {
        animationLock = true;
        animator.Play("Hop Up");
    }

    public void Bark(bool loop = false) {
        animationLock = true;
        if (loop) {
            animator.Play("Bark Loop");
        } else {
            animator.Play("Bark");
        }
    }


    public void Lick() {
        // TODO: animations
    }

    public InteractableCtrl Inspect() {
        // TODO: animations
        return GetNearestInteractable();
    }

    public InteractableCtrl GetBarkTarget() {
        return GetNearestInteractable();
    }

    public InteractableCtrl GetCrawlUnderTarget() {
        return GetNearestInteractable();
    }

    public InteractableCtrl GetLickTarget() {
        return GetNearestInteractable();
    }

    public void EnableMovement() { canMove = true; }
    public void DisableMovement() { canMove = false; }

    InteractableCtrl GetNearestInteractable(int limit = 10) {
        Collider2D[] overlappingColliders = GetCollidingObjs(body, new string[0], limit);

        // get list of all objects with requested component
        List<GameObject> nearbyInteractables = new List<GameObject>();
        for (int i = 0; i < overlappingColliders.Length; i++) {
            if (overlappingColliders[i] != null) {
                InteractableCtrl interactableCtrl = overlappingColliders[i].GetComponent<InteractableCtrl>();
                if (interactableCtrl != null) {
                    nearbyInteractables.Add(overlappingColliders[i].gameObject);
                }
            }
        }

        // get nearest object with component to player
        float nearestDistance = Mathf.Infinity;
        InteractableCtrl nearestInteractable = null;
        for (int m = 0; m < nearbyInteractables.Count; m++) {
            float distanceToPlayer = Vector3.Distance(nearbyInteractables[m].transform.position, body.transform.position);
            if (distanceToPlayer < nearestDistance) {
                nearestDistance = distanceToPlayer;
                nearestInteractable = nearbyInteractables[m].GetComponent<InteractableCtrl>();
            }
        }

        return nearestInteractable;
    }

    public BarkTriggerCtrl GetNearestBarkTrigger(int limit = 10) {
        return Utils.GetNearestComponentWithTag<BarkTriggerCtrl>(this.body, null, limit);
    }

    public HopUpTriggerCtrl GetNearestHopUpTrigger(int limit = 10) {
        return Utils.GetNearestComponentWithTag<HopUpTriggerCtrl>(this.body, null, limit);
    }

    Collider2D[] GetCollidingObjs(Rigidbody2D body, string[] layerTags = null, int limit = 20) {
        ContactFilter2D contactFilter = new ContactFilter2D();
        if (layerTags != null && layerTags.Length > 0) {
            contactFilter.layerMask = LayerMask.GetMask(layerTags);
            contactFilter.useLayerMask = true;
        }
        contactFilter.useTriggers = true;

        Collider2D[] overlappingColliders = new Collider2D[20];
        body.OverlapCollider(contactFilter, overlappingColliders);

        // remove nulls
        List<Collider2D> colliders = new List<Collider2D>(overlappingColliders);
        overlappingColliders = colliders.Where(aCollider => aCollider != null).ToArray();

        return overlappingColliders;
    }

    void OnAnimationEvent(string eventName) {
        if (eventName == "Animation End") {
            animationLock = false;
            animator.Play("Idle");
        }
    }

    void OnSFXEvent(string sfxName) {
        if (sfxName == "Bark") {
            audioSource.PlayOneShot(barkSFX);
        }
    }

}
