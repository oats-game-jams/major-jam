﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ActionPromptCtrl : MonoBehaviour {

    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private TextMeshProUGUI textUI;

    public bool IsVisible { get { return canvasGroup.alpha > 0; }}

    IEnumerator fadeRoutine = null;

    void OnDisable() {
        if (fadeRoutine != null) {
            StopCoroutine(fadeRoutine);

            canvasGroup.alpha = 0;
        }
    }

    // Update is called once per frame
    public void Show(string newText = "", bool instant = false) {
        if (canvasGroup.alpha == 1) return;

        if (fadeRoutine != null) {
            StopCoroutine(fadeRoutine);
        }

        if (newText != "") {
            textUI.text = "[SPACE] " + newText;
        }

        fadeRoutine = Utils.FadeToAlpha(canvasGroup, 1, instant ? 0f : 0.25f);
        StartCoroutine(fadeRoutine);
    }

    public void Hide(bool instant = false) {
        if (canvasGroup.alpha == 0) return;

        if (fadeRoutine != null) {
            StopCoroutine(fadeRoutine);
        }

        fadeRoutine = Utils.FadeToAlpha(canvasGroup, 0, instant ? 0f : 0.25f);
        StartCoroutine(fadeRoutine);
    }
}
