﻿using UnityEngine;
using UnityEngine.Events;

public class ExitCtrl : MonoBehaviour {

    public class ExitEvent : UnityEvent<ExitCtrl> {}

    [SerializeField] private MapCtrl.Keys toMap;
    public MapCtrl.Keys ToMap { get { return toMap; }}

    [SerializeField] private int toSpawnPoint;
    public int ToSpawnPoint { get { return toSpawnPoint; }}

    // // if true, screen happens as soon as exit is touched; otherwise,
    // // the PC must interact to move to a different area
    // [SerializeField] private bool exitOnTouch;
    // public bool ExitOnTouch { get { return exitOnTouch; }}

    // public ExitEvent OnTouch;

    // void Awake() {
    //     OnTouch = new ExitEvent();
    // }

    // void OnDisable() {
    //     OnTouch.RemoveAllListeners();
    // }

    // void OnTriggerEnter2D() {
    //     if (exitOnTouch) {
    //         OnTouch.Invoke(this);
    //     }
    // }

}
