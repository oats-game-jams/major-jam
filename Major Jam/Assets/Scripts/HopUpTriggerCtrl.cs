﻿using UnityEngine;
using UnityEngine.Events;

public class HopUpTriggerCtrl : MonoBehaviour {

    public class HopUpTriggerEvent : UnityEvent<HopUpTriggerCtrl> {}

    public enum IDs { Mailbox, KitchenTable, Bookcase }

    [SerializeField] private IDs id;
    public IDs ID { get { return id; }}

    public void TriggerEvent() {
        Destroy(this.gameObject);
    }

}

