using System.Collections.Generic;
using UnityEngine;

public class FrontDoorCtrl : InteractableCtrl {

    [SerializeField] private string beforeBark;
    [SerializeField] private string afterBark;

    public override DialogData[] Inspect(GameState gameState) {
        List<DialogData> dataList = new List<DialogData>();
        DialogData data;

        if (!gameState.barkedAtFrontDoor) {
            data = new DialogData(DialogData.DialogType.Dog, beforeBark);
            OnInteractAction.Invoke(InteractionAction.UnlockBark);
        } else {
            data = new DialogData(DialogData.DialogType.Dog, afterBark);
        }

        dataList.Add(data);

        return dataList.ToArray();
    }


}
