﻿using UnityEngine;
using UnityEngine.Events;

public class ColliderEvent : UnityEvent<Collider2D> {}

public class InclineCtrl : MonoBehaviour {

    [SerializeField] private BoxCollider2D slopeCollider;

    public ColliderEvent OnEnterSlopeArea;
    public ColliderEvent OnExitSlopeArea;

    void Start() {
        OnEnterSlopeArea = new ColliderEvent();
        OnExitSlopeArea = new ColliderEvent();
    }

    void OnDisable() {
        OnEnterSlopeArea.RemoveAllListeners();
        OnExitSlopeArea.RemoveAllListeners();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            OnEnterSlopeArea.Invoke(slopeCollider);
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Player") {
            OnExitSlopeArea.Invoke(slopeCollider);
        }
    }

}
