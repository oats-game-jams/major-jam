﻿using UnityEngine;
using UnityEngine.Events;

public class AnimationCtrl : MonoBehaviour {

    public class StringEvent : UnityEvent<string> {}

    public StringEvent OnAnimationEvent;
    public StringEvent OnSFX;

    void Awake() {
        OnAnimationEvent = new StringEvent();
        OnSFX = new StringEvent();
    }

    void OnDisable() {
        OnAnimationEvent.RemoveAllListeners();
        OnSFX.RemoveAllListeners();
    }

    public void TriggerSFX(string sfxName) {
        OnSFX.Invoke(sfxName);
    }

    public void TriggerAnimationEnd() {
        OnAnimationEvent.Invoke("Animation End");
    }

    public void DestroyOnEvent() {
        OnAnimationEvent.Invoke("Destroy");
    }
}
