using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public static class Utils {

    public static IEnumerator Delay(float duration, UnityAction OnComplete = null) {
        if (duration > 0) {
            yield return new WaitForSecondsRealtime(duration);
        }

        if (OnComplete != null) {
            OnComplete();
        }
    }

    public static IEnumerator FadeToAlpha(CanvasGroup canvasGroup, float newAlpha, float fadeDuration, UnityAction OnComplete = null) {
        bool valid = true;

        if (newAlpha < 0 || newAlpha > 1) {
            Debug.LogError(string.Format("Invalid alpha provided for FadeToAlpha: {0}", newAlpha));
            valid = false;
        }

        if (fadeDuration < 0) {
            Debug.LogError(string.Format("Invalid duration provided for FadeToAlpha: {0}", fadeDuration));
            valid = false;
        }

        if (valid) {
            float startAlpha = canvasGroup.alpha;
            float elapsedTime = 0;
            float percentComplete = 0;

            while (percentComplete < 1) {
                float nextAlpha = Mathf.Lerp(startAlpha, newAlpha, elapsedTime / fadeDuration);

                canvasGroup.alpha = nextAlpha;

                elapsedTime += Time.deltaTime;
                percentComplete = elapsedTime / fadeDuration;

                yield return null;
            }

            // set final alpha when completed
            canvasGroup.alpha = newAlpha;

            if (OnComplete != null) {
                OnComplete();
            }
        }
    }


    // for when we want to ensure the nearest object in a given layer has some
    // component
    public static T GetNearestComponentWithTag<T>(Rigidbody2D body, string[] layerTags = null, int limit = 20) {
        Collider2D[] overlappingColliders = GetCollidingObjs(body, layerTags, limit);

        // get list of all objects with requested component
        List<GameObject> nearbyInteractables = new List<GameObject>();
        for (int i = 0; i < overlappingColliders.Length; i++) {
            if (overlappingColliders[i] != null) {
                T interactableCtrl = overlappingColliders[i].GetComponent<T>();
                if (interactableCtrl != null) {
                    nearbyInteractables.Add(overlappingColliders[i].gameObject);
                }
            }
        }

        // get nearest object with component to player
        float nearestDistance = Mathf.Infinity;
        T nearestInteractable = default(T);
        for (int m = 0; m < nearbyInteractables.Count; m++) {
            float distanceToPlayer = Vector3.Distance(nearbyInteractables[m].transform.position, body.transform.position);
            if (distanceToPlayer < nearestDistance) {
                nearestDistance = distanceToPlayer;
                nearestInteractable = nearbyInteractables[m].GetComponent<T>();
            }
        }

        return nearestInteractable;
    }

    // for when we just want the closest object in a given layer
    public static Collider2D GetNearestColliderWithTag(Rigidbody2D body, string[] layerTags, int limit = 20) {
        Collider2D[] overlappingColliders = GetCollidingObjs(body, layerTags, limit);

        // remove nulls
        List<Collider2D> colliders = new List<Collider2D>(overlappingColliders);
        overlappingColliders = colliders.Where(aCollider => aCollider != null).ToArray();

        // get nearest game object to player
        float nearestDistance = Mathf.Infinity;
        Collider2D nearestInteractable = null;
        for (int m = 0; m < overlappingColliders.Length; m++) {
            float distanceToPlayer = Vector3.Distance(overlappingColliders[m].transform.position, body.transform.position);
            if (distanceToPlayer < nearestDistance) {
                nearestDistance = distanceToPlayer;
                nearestInteractable = overlappingColliders[m];
            }
        }

        return nearestInteractable;
    }

    public static Collider2D[] GetCollidingObjs(Collider2D collider, string[] layerTags = null, int limit = 20) {
        ContactFilter2D contactFilter = new ContactFilter2D();
        if (layerTags != null && layerTags.Length > 0) {
            contactFilter.layerMask = LayerMask.GetMask(layerTags);
            contactFilter.useLayerMask = true;
        }
        contactFilter.useTriggers = true;

        Collider2D[] overlappingColliders = new Collider2D[20];
        collider.OverlapCollider(contactFilter, overlappingColliders);

        // remove nulls
        List<Collider2D> colliders = new List<Collider2D>(overlappingColliders);
        overlappingColliders = colliders.Where(aCollider => aCollider != null).ToArray();

        return overlappingColliders;
    }

    public static Collider2D[] GetCollidingObjs(Rigidbody2D body, string[] layerTags = null, int limit = 20) {
        ContactFilter2D contactFilter = new ContactFilter2D();
        if (layerTags != null && layerTags.Length > 0) {
            contactFilter.layerMask = LayerMask.GetMask(layerTags);
            contactFilter.useLayerMask = true;
        }
        contactFilter.useTriggers = true;

        Collider2D[] overlappingColliders = new Collider2D[20];
        body.OverlapCollider(contactFilter, overlappingColliders);

        // remove nulls
        List<Collider2D> colliders = new List<Collider2D>(overlappingColliders);
        overlappingColliders = colliders.Where(aCollider => aCollider != null).ToArray();

        return overlappingColliders;
    }

    public static T GetClosestObj<T>(Rigidbody2D body, Collider2D[] objects) {
        float nearestDistance = Mathf.Infinity;
        T nearestInteractable = default(T);
        for (int m = 0; m < objects.Length; m++) {
            float distanceToPlayer = Vector3.Distance(objects[m].transform.position, body.transform.position);
            if (distanceToPlayer < nearestDistance) {
                nearestDistance = distanceToPlayer;
                nearestInteractable = objects[m].GetComponent<T>();
            }
        }

        return nearestInteractable;
    }

    public static IEnumerator FadeOut(AudioSource audioCtrl, float fadeDuration, UnityAction OnComplete = null) {
            float t = 0;

            float startVolume = audioCtrl.volume;

            while (t < fadeDuration) {
                float nextVolume = Mathf.Lerp(startVolume, 0f, t / fadeDuration);
                audioCtrl.volume = nextVolume;

                t += Time.deltaTime;
                yield return null;
            }

            if (OnComplete != null) {
                OnComplete();
            }
        }

        public static IEnumerator FadeIn(AudioSource audioCtrl, float fadeDuration, float fadeTo = 1f, UnityAction OnComplete = null) {
            float t = 0;

            float startVolume = audioCtrl.volume;

            while (t < fadeDuration) {
                audioCtrl.volume = Mathf.Lerp(startVolume, fadeTo, t / fadeDuration);

                t += Time.deltaTime;
                yield return null;
            }

            if (OnComplete != null) {
                OnComplete();
            }
        }

}